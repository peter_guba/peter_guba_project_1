local sensorInfo = {
	name = "GetPrevPointPos",
	desc = "Returns the position of the nth previous corridor point. (I used this in the NOTA exam.)",
	author = "Peter Guba",
	date = "2020-07-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(info, n, current)
	return info.corridors[current.corridor].points[current.index - n].position
end