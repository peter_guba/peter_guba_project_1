local sensorInfo = {
	name = "SortByDistance",
	desc = "Sorts a given array of places by distance from a given origin.",
	author = "Peter Guba",
	date = "2020-06-05",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(origin, places)
	local result = {}
	local originVector
	
	if type(origin) ~= "table" then		-- If origin is a unit, get its position.
		local x, y, z = Spring.GetUnitPosition(origin)
		originVector = Vec3(x, y, z)
	else	-- If it is a table, use its values to make a vector.
		originVector = Vec3(origin.x, origin.y, origin.z)
	end

	result[1] = places[1]
	result[1].d = originVector:Distance(Vec3(places[1].x, places[1].y, places[1].z))

	for i = 2, #places do	-- Sort using insert sort.
		local place = Vec3(places[i].x, places[i].y, places[i].z)
		local dist = originVector:Distance(place)
		
		local count = #result
		while count > 0 and dist < result[count].d do
			count = count - 1
		end
		
		table.insert(result, count + 1, places[i])
		result[count + 1].d = dist
	end
	
	return result
end