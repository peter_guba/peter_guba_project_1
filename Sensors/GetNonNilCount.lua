local sensorInfo = {
	name = "GetNonNilCount",
	desc = "Returns the number of table elements that aren't nil.",
	author = "Peter Guba",
	date = "2020-07-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(t, length)
	if t == nil then 
		return 0
	end

	local count = 0

	for i = 1, length do
		if t[i] ~= nil then
			count = count + 1
		end
	end
	
	return count
end