local sensorInfo = {
	name = "UpdateUnexplored",
	desc = "Updates the grid of unexplored squares.",
	author = "Peter Guba",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unexplored, squareSize)	-- Marks squares around the first unit as explored.
	if #units ~= 0 then
		local radius = 500	-- This name can be a little misleading, because the area affected by this is actually a square, not a circle.
		local x, y, z = Spring.GetUnitPosition(units[1])
	
		for i = math.max(math.ceil((x - radius) / squareSize), 1), math.min(math.ceil((x + radius) / squareSize), #unexplored) do
			for j = math.max(math.ceil((z - radius) / squareSize), 1), math.min(math.ceil((z + radius) / squareSize), #unexplored[1]) do
				unexplored[i][j] = 0
			end
		end
	end
	
	return unexplored
end