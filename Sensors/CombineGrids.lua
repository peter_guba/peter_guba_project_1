local sensorInfo = {
	name = "CombineGrids",
	desc = "Combines two grids, one indicating danger levels and the other indicating which parts of the map are unexplored.",
	author = "Peter Guba",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unexplored, danger)
	local result = {}

	for i = 1, #unexplored do
		result[i] = {}
		for j = 1, #unexplored[1] do
			if unexplored[i][j] == -1 then	-- The unexplored squares will have a score of -1. This makes A* perceive them as shortening the path,
											-- thereby "motivating" units to explore them.
				result[i][j] = -1
			else
				result[i][j] = danger[i][j]
			end
		end
	end
	
	return result
end