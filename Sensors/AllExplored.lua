local sensorInfo = {
	name = "AllExplored",
	desc = "Finds out whether there are any more uexplored squares.",
	author = "Peter Guba",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unexplored)
	for i = 1, #unexplored do
		for j = 1, #unexplored[1] do
			if unexplored[i][j] < 0 then
				return false
			end
		end
	end
	
	return true
end