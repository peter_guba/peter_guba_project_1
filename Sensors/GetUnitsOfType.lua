local sensorInfo = {
	name = "DebugGrid",
	desc = "Selects a unit in the proximity of units[1]. Used for testing.",
	author = "Peter Guba",
	date = "2020-07-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(unitDefName)
	local id = UnitDefNames[unitDefName].id
	
	return Spring.GetTeamUnitsByDefs(0, id)
end