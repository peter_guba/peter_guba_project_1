local sensorInfo = {
	name = "GetUnexploredSquares",
	desc = "Gets a grid with squares which cannot be seen by the radar marked (their value is -1 instead of 0).",
	author = "Peter Guba",
	date = "2020-06-06",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local IsPosInRadar = Spring.IsPosInRadar
local GetGroundHeight = Spring.GetGroundHeight

return function(squares, squareSize)
	result = {}
	
	for i = 1, #squares do
		result[i] = {}
		for j = 1, #squares[1] do
			local posX = (i - 1) * squareSize
			local posZ = (j - 1) * squareSize
			if not IsPosInRadar(posX, GetGroundHeight(posX, posZ), posZ, 0) then
				result[i][j] = -1
			else
				result[i][j] = 0
			end
		end
	end
	
	return result
end