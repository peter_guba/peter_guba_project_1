local sensorInfo = {
	name = "InterestingUnits",
	desc = "Returns a table of units that can be picked up and returned to the safe area.",
	author = "Peter Guba",
	date = "2020-05-19",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

-- speed-ups
local GetTeamUnits = Spring.GetTeamUnits
local GetUnitDefID = Spring.GetUnitDefID
local GetUnitPosition = Spring.GetUnitPosition

return function()
	u = GetTeamUnits(0)
	interestingUnits = {}
		
	for i = 1, #u do
		name = UnitDefs[GetUnitDefID(u[i])].name
		if(name == "armbox" or name == "armbull" or name == "armrock" or name == "armmllt" or name == "armham") then
			x, y, z = GetUnitPosition(u[i])
			interestingUnits[#interestingUnits + 1] = {["id"] = u[i], ["x"] = x, ["y"] = y, ["z"] = z, ["deaths"] = 0}
		end
	end
	
	return interestingUnits
end