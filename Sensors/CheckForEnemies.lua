local sensorInfo = {
	name = "CheckForEnemies",
	desc = "Scans the unit's surroundings for enemies.",
	author = "Peter Guba",
	date = "2020-07-02",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetUnitHealth = Spring.GetUnitHealth
local GetNearestEnemy = Spring.GetUnitNearestEnemy
local ValidID = Spring.ValidUnitID

-- local prevHealth = 0

return function(unit, radius)
	if not ValidID(unit) then
		return true
	end

	-- if GetNearestEnemy(unit, radius) then
		-- Spring.Echo("Oh Lo'd")
	-- else
		-- Spring.Echo(prevHealth)
	-- end

	-- if GetUnitHealth(unit) < prevHealth or GetNearestEnemy(unit, radius) then
		-- return true
	-- end
	
	-- prevHealth = GetUnitHealth(unit)
	
	-- return false

	if GetNearestEnemy(unit, radius) then
		-- prevHealth = GetUnitHealth(unit)
		return true
	else	
		-- prevHealth = GetUnitHealth(unit)
		return false
	end
end