local sensorInfo = {
	name = "DebugPath",
	desc = "Displays a pre-computed path.",
	author = "Peter Guba",
	date = "2020-05-28",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message")

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(path, height, squareSize)
	if(Script.LuaUI('drawSquaresDebug_update')) then
		for i = 1, #path do
			x = path[i].x
			z = path[i].y
			Script.LuaUI.drawSquaresDebug_update(
					-i,	-- I'm using the negative value here because I usually used this in conjunction with drawing the grid, so I needed
						-- the path squares to have distinct ids.
					{
						r = 0,
						g = 1,
						b = 0,
						opacity = 0.7,
						point1 = Vec3(x * squareSize, height, z * squareSize),
						point2 = Vec3(x * squareSize, height, z * squareSize + squareSize),
						point3 = Vec3(x * squareSize + squareSize, height, z * squareSize + squareSize),
						point4 = Vec3(x * squareSize + squareSize, height, z * squareSize)
					}
				)
		end
	end
end