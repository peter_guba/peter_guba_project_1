local sensorInfo = {
	name = "DebugGrid",
	desc = "Displays the pre-computed map grid and the scores of its squares (as colours).",
	author = "Peter Guba",
	date = "2020-05-26",
	license = "notAlicense",
}

-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message")

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(squares, height, squareSize)
	if(Script.LuaUI('drawSquaresDebug_update')) then
		local counter = 1	-- This counter serves to give every square a unique id.
		for k = 0, #squares - 1 do	-- Go through all of the squares using this double for cycle and send their data to the debugging widget.
			for l = 0, #squares[1] - 1 do
					local opacity, r, g, b
					if squares[k + 1][l + 1] >= 0 then
						opacity = (squares[k + 1][l + 1] - 5) / 200
						r = 1
						g = 0
						b = 0
					else
						opacity = 0.3
						r = 0
						g = 0
						b = 1
					end
					
					Script.LuaUI.drawSquaresDebug_update(
						counter,
						{
							r = r,
							g = g,
							b = b,
							opacity = opacity,
							point1 = Vec3(k * squareSize, height, l * squareSize),
							point2 = Vec3(k * squareSize, height, l * squareSize + squareSize),
							point3 = Vec3(k * squareSize + squareSize, height, l * squareSize + squareSize),
							point4 = Vec3(k * squareSize + squareSize, height, l * squareSize),
						}
					)
				
				counter = counter + 1
			end
		end
	end
	
	return nil
end