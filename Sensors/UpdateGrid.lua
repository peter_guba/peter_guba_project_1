local sensorInfo = {
	name = "UpdateGrid",
	desc = "Scans the unit's surroundings for enemies and updates the gird based on that.",
	author = "Peter Guba",
	date = "2020-05-31",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetUnitsInCylinder = Spring.GetUnitsInCylinder
local GetUnitPosition = Spring.GetUnitPosition
local GetGroundHeight = Spring.GetGroundHeight
local GetUnitHealth = Spring.GetUnitHealth
local GetUnitRadius = Spring.GetUnitRadius

local radius = 1000		-- The radius of effect that the enemies should have on the grid.
local prevID = nil		-- The id of the unit in the previous tick. Used to check if the unit is still alive.
local prevHealth = -100	-- The unit's health in the previous tick. Used to check whether the unit was attacked.
local prevX, prevY, prevZ	-- The unit's position in the previous tick. Used when the unit is destroyed to mark the area in which it died as dangerous.

-- Converts map coordinates to grid coordinates.
function MapToGrid(x, z, squareSize)
	return math.ceil(x / squareSize), math.ceil(z / squareSize)
end

function GetDistance(x1, y1, x2, y2)
	local first = Vec3(x1, GetGroundHeight(x1, y1), y1)
	local second = Vec3(x2, GetGroundHeight(x2, y2), y2)
	
	return first:Distance(second)
end

return function(squares, squareSize, threshold, enemyTeam, accountForDeath, interestingUnits, count)
	if #units == 0 then
		return squares
	end

	local x, y, z = GetUnitPosition(units[1])
	local enemies = GetUnitsInCylinder(x, z, 1000, enemyTeam)
	
	if prevID == units[1] and prevHealth ~= -100 and prevHealth > GetUnitHealth(units[1]) then	-- If the units health was decreased, increase the score around its current position.
		local nx, nz = MapToGrid(x, z, squareSize)
		if nx > #squares then
			nx = #squares
		end
		
		if nz > #squares[1] then
			nz = #squares[1]
		end
		
		for i = math.max(nx - 1, 1), math.min(nx + 1, #squares) do
			for j = math.max(nz - 1, 1), math.min(nz + 1, #squares[1]) do
				squares[i][j] = squares[i][j] + math.abs((math.abs(nx - i) + math.abs(nz - j)) - 3) * 10
			end
		end
	end
	
	prevHealth = GetUnitHealth(units[1])
	
	if(accountForDeath and prevID ~= nil and prevID ~= units[1]) then	-- If the unit was destroyed, increase the score of the area where it happened and increase the death count of the unit it was after.
		local sx, sz = MapToGrid(prevX, prevZ, squareSize)
		if sx > #squares then
			sx = #squares
		end
		
		if sz > #squares[1] then
			sz = #squares[1]
		end
		
		local newRadius = radius
		squares[sx][sz] = squares[sx][sz] + 100
			
		for a = math.max(prevX - newRadius + squareSize / 2, 0), math.min(prevX + newRadius - squareSize / 2, #squares * squareSize), squareSize do
			for b = math.max(prevZ - newRadius + squareSize / 2, 0), math.min(prevZ + newRadius - squareSize / 2, #squares[1] * squareSize), squareSize do
				local dist = GetDistance(prevX + squareSize / 2, prevZ + squareSize / 2, a, b)
				if(dist <= newRadius) then
					aIndex = math.ceil(a / squareSize)
					bIndex = math.ceil(b / squareSize)
						
					if(aIndex > 0 and bIndex > 0 and aIndex <= #squares and bIndex <= #squares[aIndex]) then
						local amount = math.min(math.ceil(newRadius / dist), 4)
						squares[aIndex][bIndex] = squares[aIndex][bIndex] + amount * 10
					end
				end
			end
		end
		
		interestingUnits[count].deaths = interestingUnits[count].deaths + 1
	end
	
	prevID = units[1]
	prevX = x
	prevY = y
	prevZ = z
	
	for i = 1, #enemies do	-- For each new enemy, increase the score of its surrounding area.
		local r = GetUnitRadius(enemies[i])
	
		local newR
		if r == nil then
			newR = r
		else
			newR = radius
		end
		
		local ex, ey, ez = GetUnitPosition(enemies[i])
		local sx, sz = MapToGrid(ex, ez, squareSize)
		if squares[sx][sz] < threshold then
			squares[sx][sz] = squares[sx][sz] + 100
		end
		for a = ex - radius + squareSize / 2, ex + radius - squareSize / 2, squareSize do
			for b = ez - radius + squareSize / 2, ez + radius - squareSize / 2, squareSize do
				local dist = GetDistance(ex + squareSize / 2, ez + squareSize / 2, a, b)
				local angle = math.deg(math.asin((GetGroundHeight(sx, sz) - GetGroundHeight(a, b))/dist))
				
				if(dist <= radius and angle < 45) then
					aIndex = math.ceil(a / squareSize)
					bIndex = math.ceil(b / squareSize)
						
					if aIndex > 0 and bIndex > 0 and aIndex <= #squares and bIndex <= #squares[aIndex] and squares[aIndex][bIndex] < threshold then
						if r == nil then
							squares[aIndex][bIndex] = squares[aIndex][bIndex] + 50
						else
							squares[aIndex][bIndex] = squares[aIndex][bIndex] + 100
						end
					end
				end
			end
		end
	end
	
	return squares
end