local sensorInfo = {
	name = "DivideUnits",
	desc = "Divides the interesting units into a given amount of parts.",
	author = "Peter Guba",
	date = "2020-05-31",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(interestingUnits, numOfParts)
	local result = {}
	
	for i = 1, numOfParts do
		result[i] = {}
	end
	
	local toDivide
	if interestingUnits == nil then
		toDivide = units
	else
		toDivide = interestingUnits
	end
	
	local j = 0
	for i = 1, #toDivide do
		j = math.fmod(j, numOfParts) + 1
		result[j][#result[j] + 1] = toDivide[i]
	end
	
	return result
end