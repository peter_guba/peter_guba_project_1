local sensorInfo = {
	name = "DebugGrid",
	desc = "Selects a unit in the proximity of units[1]. Used for testing.",
	author = "Peter Guba",
	date = "2020-07-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	x, y, z = Spring.GetUnitPosition(units[1])
	candidates = Spring.GetUnitsInCylinder(x, z, 1000)
	
	return candidates[1]
end