local sensorInfo = {
	name = "GetNonNilCount",
	desc = "Updates the nil slots in a unit table. (I used these in the NOTA exam.)",
	author = "Peter Guba",
	date = "2020-07-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local ValidID = Spring.ValidUnitID

function contains(t, element)
  for _, value in pairs(t) do
    if value == element then
      return true
    end
  end
  return false
end

return function(unitTable, length, unitDefName)
	local id = UnitDefNames[unitDefName].id
	local unitsOfType = Spring.GetTeamUnitsByDefs(0, id)
	
	if unitTable == nil then
		if unitsOfType == nil then
			return {}
		else
			return unitsOfType
		end
	end
	
	local updateCount = 0
	
	for i = 1, length do
		if unitTable[i] == nil then
			if (not contains(unitTable, unitsOfType[i])) and ValidID(unitsOfType[i]) then
				unitTable[i] = unitsOfType[i]
			elseif not contains(unitTable, unitsOfType[#unitsOfType - updateCount]) then
				unitTable[i] = unitsOfType[#unitsOfType - updateCount]
				updateCount = updateCount + 1
			else
				break
			end
		end
	end	
	
	-- local updateCount = 0
	
	-- for i = 1, length do
		-- if unitTable[i] == nil then
			-- if not contains(unitTable, unitsOfType[#unitsOfType - updateCount]) then
				-- unitTable[i] = unitsOfType[#unitsOfType - updateCount]
				-- updateCount = updateCount + 1
			-- else
				-- break
			-- end
		-- end
	-- end
	
	return unitTable
end