local sensorInfo = {
	name = "DebugGrid",
	desc = "Checks how much metal is in a given area.",
	author = "Peter Guba",
	date = "2020-07-08",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetFeatureResources = Spring.GetFeatureResources

return function(x, z, radius)
	features = Spring.GetFeaturesInCylinder(x, z, radius)
	
	metal = 0
	for i = 1, #features do
		metal = metal + GetFeatureResources(features[i])
	end
	
	return metal
end