local sensorInfo = {
	name = "UpdateBattleLineIndices",
	desc = "Updates the indeces of the last ally-owned strongpoint. (I used this in the NOTA exam.)",
	author = "Peter Guba",
	date = "2020-07-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function(corridors)
	names = {'Top', 'Middle', 'Bottom'}
	result = {}
	
	for i = 1, 3 do
		c = corridors[names[i]].points
		for j = 1, #c do
			if c[j].ownerAllyID ~= nil and c[j].ownerAllyID == 1 then
				result[i] = {}
				result[i]['index'] = j - 1
				result[i]['corridor'] = names[i]
				break
			end
		end
	end
	
	return result
end