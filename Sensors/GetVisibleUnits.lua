local sensorInfo = {
	name = "VisibleUnits",
	desc = "Returns the number of units in a given radius.",
	author = "Peter Guba",
	date = "2020-05-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1 -- acutal, no caching

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT 
	}
end

return function(unitID, radius)
	local x, y, z = Spring.GetUnitPosition(unitID)
	return Spring.GetUnitsInCylinder(x, z, radius)
end