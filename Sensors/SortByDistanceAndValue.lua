local sensorInfo = {
	name = "SortByDistanceAndValue",
	desc = "Sorts a given array of units by distance from a given origin and by their value.",
	author = "Peter Guba",
	date = "2020-06-05",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetUnitDefID = Spring.GetUnitDefID

function GetValue(id)
	local name = UnitDefs[GetUnitDefID(id)].name
	if name == "armbox" then
		return 10
	elseif name == "armmllt" then
		return 5
	else
		return 1
	end
end

return function(origin, unitsList)
	local result = {}
	local originVector
	
	if type(origin) ~= "table" then		-- If origin is a unit, get its position.
		local x, y, z = Spring.GetUnitPosition(origin)
		originVector = Vec3(x, y, z)
	else	-- If it is a table, use its values to make a vector.
		originVector = Vec3(origin.x, origin.y, origin.z)
	end

	result[1] = unitsList[1]
	result[1].weight = originVector:Distance(Vec3(unitsList[1].x, unitsList[1].y, unitsList[1].z)) - GetValue(unitsList[1].id) * 100

	for i = 2, #unitsList do	-- Sort using insert sort.
		local unitPos = Vec3(unitsList[i].x, unitsList[i].y, unitsList[i].z)
		local weight = originVector:Distance(unitPos) - GetValue(unitsList[1].id) * 100
		
		local count = #result
		while count > 0 and weight < result[count].weight do
			count = count - 1
		end
		
		table.insert(result, count + 1, unitsList[i])
		result[count + 1].weight = weight
	end
	
	return result
end