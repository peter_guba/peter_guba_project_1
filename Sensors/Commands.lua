local sensorInfo = {
	name = "Commands",
	desc = "Gets a unit's commands.",
	author = "Peter Guba",
	date = "2020-05-31",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GiveOrder = Spring.GiveOrderToUnitArray
local GetUnitCommands = Spring.GetUnitCommands
local GetUnitCommandQueue = Spring.GetCommandQueue

return function()
	return GetUnitCommandQueue(units[1])
end