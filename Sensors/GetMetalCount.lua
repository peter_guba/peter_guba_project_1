local sensorInfo = {
	name = "GetMetalCount",
	desc = "Returns the amount of metal you currently have.",
	author = "Peter Guba",
	date = "2020-07-10",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

return function()
	currentLevel = Spring.GetTeamResources(0, "metal")
	return currentLevel
end