local sensorInfo = {
	name = "CheckForEnemies",
	desc = "Scans the given area for enemies.",
	author = "Peter Guba",
	date = "2020-07-02",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetUnitsInCylinder = Spring.GetUnitsInCylinder
local GetUnitTeam = Spring.GetUnitTeam

local prevHealths = {}

return function(x, z, radius)
	for unit in GetUnitsInCylinder(x, z, radius) do
		if GetUnitTeam(unit) != GetUnitTeam(units[1]) then
			return true
		end
	end
	
	return false
end