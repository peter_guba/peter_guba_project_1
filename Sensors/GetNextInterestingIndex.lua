local sensorInfo = {
	name = "AllExplored",
	desc = "Finds the next index of a still existing unit",
	author = "Peter Guba",
	date = "2020-06-07",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetUnitIsDead = Spring.GetUnitIsDead
local ValidUnitID = Spring.ValidUnitID

return function(count, interestingUnits)
	while (count <= #interestingUnits and ((not ValidUnitID(interestingUnits[count].id)) or GetUnitIsDead(interestingUnits[count].id) == nil or GetUnitIsDead(interestingUnits[count].id))) do	-- If the unit at index "count" has been destroyed, increment count.
		count = count + 1
	end
	
	increment = 0
	while increment + count + 1 <= #interestingUnits and interestingUnits[count + increment].deaths > interestingUnits[count + increment + 1].deaths do
		interestingUnits[count + increment], interestingUnits[count + increment + 1] = interestingUnits[count + increment + 1], interestingUnits[count + increment]
	end
	
	return count
end