local sensorInfo = {
	name = "GetScoredGrid",
	desc = "Returns a grid, each square of which has a score attached to it which expresses how much danger there is on the map in that place.",
	author = "Peter Guba",
	date = "2020-05-26",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = -1

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed-ups
local GetUnitsInRectangle = Spring.GetUnitsInRectangle
local GetTeam = Spring.GetUnitTeam
local GetGroundHeight = Spring.GetGroundHeight

function GetDistance(x1, y1, x2, y2)
	local first = Vec3(x1, GetGroundHeight(x1, y1), y1)
	local second = Vec3(x2, GetGroundHeight(x2, y2), y2)
	
	return first:Distance(second)
end

return function(squareSize, mapX, mapZ, enemyTeam)
	local squares = {}	-- The resulting grid.
	local radius = 1000	-- The radius of effect that an enemy unit should have on the grid.
	
	Spring.Echo("wtf")
	
	
	for i = 0, mapX, squareSize do	-- Go through the entire map using this double for cycle.
		squares[#squares + 1] = {}
		for j = 0, mapZ, squareSize do			
			adjustment = 0
			if(GetGroundHeight(i + squareSize / 2, j + squareSize / 2) < 50) then	-- If the ground is really low, it should have a lower score, since units usually can't shoot there (on the given map).
				adjustment  = -5
			end
			
			squares[#squares][#squares[#squares] + 1] = 5 + adjustment
			
			enemies = GetUnitsInRectangle(i, j, i + squareSize, j + squareSize, enemyTeam)	-- Check whether there are any enemies in the given square and if so, adjust the score.
			if(enemies ~= nil) then
				for k = 1, #enemies do
					squares[#squares][#squares[#squares]] = squares[#squares][#squares[#squares]] + 100
					
					for a = i - radius + squareSize / 2, i + radius - squareSize / 2, squareSize do
						for b = j - radius + squareSize / 2, j + radius - squareSize / 2, squareSize do
							local dist = GetDistance(i + squareSize / 2, j + squareSize / 2, a, b)
							local angle = math.deg(math.asin((GetGroundHeight(i + squareSize / 2, j + squareSize / 2) - GetGroundHeight(a, b))/dist))
							
							if(dist <= radius and angle < 45) then
								aIndex = math.ceil(a / squareSize)
								bIndex = math.ceil(b / squareSize)
								
								if(aIndex > 0 and bIndex > 0 and aIndex <= #squares and bIndex <= #squares[aIndex]) then
									squares[aIndex][bIndex] = squares[aIndex][bIndex] + 50
								end
							end
						end
					end
				end
			end
		end
	end
	
	return squares
end