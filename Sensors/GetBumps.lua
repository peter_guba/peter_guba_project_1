local sensorInfo = {
	name = "GetBumps",
	desc = "Finds the positions of bumps on the map.",
	author = "Peter Guba",
	date = "2020-05-12",
	license = "notAlicense",
}

local EVAL_PERIOD_DEFAULT = 0 -- since the position of the bumps shouldn't change over time, caching is ok

function getInfo()
	return {
		period = EVAL_PERIOD_DEFAULT
	}
end

-- speed up
local GetHeight = Spring.GetGroundHeight

return function(mapX, mapZ, stepX, stepZ, elevation, baseElevation)
	positions = {}

	-- It doesn't start at position [0, 0] because otherwise some bots would end up 
	-- standing on the edges of bumps and the game wouldn't register the bumps as taken.
	for x = 20, mapX, stepX do
		for z = 20, mapZ, stepZ do
			if GetHeight(x, z) == elevation then
				table.insert(positions, {["x"] = x, ["z"] = z})
			end
		end
	end
	
	return positions
end