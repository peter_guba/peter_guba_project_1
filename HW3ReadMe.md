The A* algorithm source code I downloaded can be found here: https://github.com/lattejed/a-star-lua
It is shared with the permission of the author (as can be seen in the Copyright statement included in the script).

The icon I used was downloaded from here: https://pngtree.com/freepng/yellow-parchment-treasure-map_4716471.html
It was downloaded with a licence for commercial use (which I hope includes using it here).