function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Loads a single specified unit",
		parameterDefs = {
			{
				name = "target",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = {}
			},
			{
				name = "assignedUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
		}
	}
end

-- speed-ups
local GetUnitTransporter = Spring.GetUnitTransporter
local GiveOrder = Spring.GiveOrderToUnit
local ValidID = Spring.ValidUnitID

local function ClearState(self)
	self.initialization = false
	self.prevID = nil	-- The id of the unit in the previous tick. Used to check whether the unit is still alive.
end
function Run(self, units, parameters)
	local targetID = parameters.target["id"]
	local assignedUnit = parameters.assignedUnit
	
	if not self.initialization then
		if Spring.GetUnitIsTransporting(assignedUnit) ~= nil and #Spring.GetUnitIsTransporting(assignedUnit) ~= 0 then
			return SUCCESS
		end
	
		GiveOrder(assignedUnit, CMD.LOAD_UNITS, {targetID}, {""})
		self.initialization = true
	end
	
	if self.prevID ~= assignedUnit and self.prevID ~= nil then
		return FAILURE
	end
	
	if GetUnitTransporter(targetID) == nil and ValidID(targetID) then
		return RUNNING
	end
	
	return SUCCESS
end

function Reset(self)
	ClearState(self)
end