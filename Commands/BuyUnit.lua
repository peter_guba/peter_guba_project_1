-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = RUNNING, -- instant success
        tooltip = "Buy unit in param",
        parameterDefs = {
            { 
                name = "unitName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "'armbox'",
            },
			{
				name = "price",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
        }
    }
end

-- speed-ups
local GetTeamResources = Spring.GetTeamResources

function Run(self, units, parameters)
	if GetTeamResources(0, "metal") < parameters.price then
		return FAILURE
	end
	
    message.SendRules({
        subject = "swampdota_buyUnit",
        data = {
            unitName = parameters.unitName
        },
    })

    return SUCCESS
end


function Reset(self)
    return self
end