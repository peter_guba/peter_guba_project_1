function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Move to defined position",
		parameterDefs = {
			{ 
				name = "position",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "",
			},
			{
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
		}
	}
end

-- constants
local THRESHOLD_STEP = 25
local THRESHOLD_DEFAULT = 10
local mapSizeX = Game.mapSizeX
local mapSizeZ = Game.mapSizeZ
local BOUNDARY = 100

-- speed-ups
local SpringGetUnitPosition = Spring.GetUnitPosition
local SpringGiveOrderToUnit = Spring.GiveOrderToUnit

local function ClearState(self)
	self.threshold = THRESHOLD_DEFAULT
	self.lastPointmanPosition = Vec3(0,0,0)
end

function Run(self, units, parameter)
	local position = parameter.position -- Vec3
	local x = math.min(position.x, Game.mapSizeX - BOUNDARY)
	x = math.max(x, 0 + BOUNDARY)
	local z = math.min(position.z, Game.mapSizeZ - BOUNDARY)
	z = math.max(z, 0 + BOUNDARY)
	y = Spring.GetGroundHeight(x,z)
	position = Vec3(x,y,z)
	
	-- pick the spring command implementing the move
	local cmdID = CMD.MOVE

	local pointman = parameter.unit
	local pointX, pointY, pointZ = SpringGetUnitPosition(pointman)
	local pointmanPosition = Vec3(pointX, pointY, pointZ)
	
	-- threshold of pointan success
	if (pointmanPosition == self.lastPointmanPosition) then 
		self.threshold = self.threshold + THRESHOLD_STEP 
	else
		self.threshold = THRESHOLD_DEFAULT
	end
	self.lastPointmanPosition = pointmanPosition
	
	-- check pointman success
	-- THIS LOGIC IS TEMPORARY, NOT CONSIDERING OTHER UNITS POSITION
	if (pointmanPosition:Distance(position) <= self.threshold) then
		return SUCCESS
	else
		SpringGiveOrderToUnit(pointman, cmdID, position:AsSpringVector(), {})
		
		return RUNNING
	end
end


function Reset(self)
	ClearState(self)
end
