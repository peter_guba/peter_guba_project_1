function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Unloads the transporters unit in a specified area.",
		parameterDefs = {
			{
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "y",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "assignedUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
		}
	}
end

-- speed-ups
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GiveOrder = Spring.GiveOrderToUnit
local GetUnitsInSphere = Spring.GetUnitsInSphere
local GetUnitPosition = Spring.GetUnitPosition

local TICKS_TO_WAIT = 10	-- The number of ticks that the transporter should wait before unloading (because of the sliding problem).

local function ClearState(self)
	self.initialization = false
	self.goal = Vec3(-1, 0, -1)
end

function Run(self, units, parameters)
	if not self.initialization then
		local x, y, z = parameters.x, parameters.y, parameters.z	
		self.goal = Vec3(x, 0, z)
		GiveOrder(parameters.assignedUnit, CMD.MOVE, {x, y, z}, {""})	-- First move to the spot.
		self.initialization = true
	end
	
	local posX, posY, posZ = GetUnitPosition(parameters.assignedUnit)
	local pos = Vec3(posX, 0, posZ)
	if pos:Distance(self.goal) ~= nil and pos:Distance(self.goal) < 50 then
		GiveOrder(parameters.assignedUnit, CMD.UNLOAD_UNITS, {self.goal.x, parameters.y, self.goal.z, 200}, {""})
	end
	
	if GetUnitIsTransporting(parameters.assignedUnit) ~= nil and #GetUnitIsTransporting(parameters.assignedUnit) ~= 0 then
		return RUNNING
	end
	
	return SUCCESS
end

function Reset(self)
	ClearState(self)
end