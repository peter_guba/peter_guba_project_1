function getInfo()
	return {
		onNoUnits = RUNNING, -- instant success
		tooltip = "Unloads the transporters unit in a specified area.",
		parameterDefs = {
			{
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "y",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "assignedUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
		}
	}
end

-- speed-ups
local GetUnitIsTransporting = Spring.GetUnitIsTransporting
local GiveOrder = Spring.GiveOrderToUnit
local GetUnitsInSphere = Spring.GetUnitsInSphere
local GetUnitPosition = Spring.GetUnitPosition

local TICKS_TO_WAIT = 10	-- The number of ticks that the transporter should wait before unloading (because of the sliding problem).

local function ClearState(self)
	self.initialization = false
	self.goal = Vec3(-1, 0, -1)
	self.countdown = TICKS_TO_WAIT
end

function Run(self, units, parameters)
	if not self.initialization then
		local x, y, z = GetSpot(parameters.x, parameters.y, parameters.z, parameters.radius)		
		self.goal = Vec3(x, 0, z)
		GiveOrder(parameters.assignedUnit, CMD.MOVE, {x, y, z}, {""})	-- First move to the spot.
		self.initialization = true
	end
	
	local posX, posY, posZ = GetUnitPosition(parameters.assignedUnit)
	local pos = Vec3(posX, 0, posZ)
	if pos:Distance(self.goal) ~= nil and self.countdown ~= nil and pos:Distance(self.goal) < 50 and self.countdown > 0 then		-- If the unit is near its destination, decrement the countdown.
		self.countdown = self.countdown - 1
	end
		
	if self.countdown == 0 then		-- When the countdown has reached 0, unload the unit.
		GiveOrder(parameters.assignedUnit, CMD.UNLOAD_UNIT, {self.goal.x, parameters.y, self.goal.z}, {""})
		self.countdown = -1
	end
	
	if GetUnitIsTransporting(parameters.assignedUnit) ~= nil and #GetUnitIsTransporting(parameters.assignedUnit) ~= 0 then
		return RUNNING
	end
	
	return SUCCESS
end

function GetSpot(x, y, z, radius)	-- Finds an empty spot to put the unit down.
	side = radius / math.sqrt(2) - 20
	newX = x - side
	newZ = z - side
	
	while #GetUnitsInSphere(newX, y, newZ, 100) ~= 0 do
		newX = newX + 125
		if newX > x + side then
			newX = x - side
			newZ = newZ + 125
		end
	end
	
	return newX, y, newZ
end

function Reset(self)
	ClearState(self)
end