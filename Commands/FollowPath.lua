function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Makes the unit follow a given path (potentially in reverse).",
		parameterDefs = {
			{
				name = "path",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = {}
			},
			{
				name = "assignedUnit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "pathUnitSize",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 1
			},
			{
				name = "reverse",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = "false"
			}
		}
	}
end

-- speed-ups
local GiveOrder = Spring.GiveOrderToUnit
local GetUnitPosition = Spring.GetUnitPosition
local GetUnitIsDead = Spring.GetUnitIsDead
local ValidUnitID = Spring.ValidUnitID
local GetUnitHealth = Spring.GetUnitHealth

local THRESHOLD_DEFAULT = 250	-- Threshold is the minimal distance that the unit has to have from its goal for this behaviour to succeed.
								-- This is its default value.
local THRESHOLD_STEP = 10		-- This is the value by which the threshold should increase if the unit stops moving.

local function ClearState(self)
	self.initialization = true
	self.prevID = nil	-- The id of the assigned unit in the previous tick. Used to check whether the unit has been destroyed.
	self.threshold = THRESHOLD_DEFAULT
	self.prevPos = Vec3(-1, -1, -1)	-- The position of the unit in the previous tick. Used to check whether the unit has stopped moving.
end

function Run(self, units, parameters)
	local assignedUnit = parameters.assignedUnit
	
	if self.initialization and parameters.path == nil then
		return FAILURE
	end
	
	if self.initialization then		-- Calculate the unit's destination (goal) and command it to follow the given path.
		self.prevID = assignedUnit
		local path = parameters.path
		local pathUnitSize = parameters.pathUnitSize
	
		local initValue
	    local finalValue
		local step
		if parameters.reverse then
			initValue = #path
			finalValue = 1
			step = -1
			self.goal = Vec3(path[1].x * pathUnitSize - pathUnitSize / 2, 0, path[1].y * pathUnitSize - pathUnitSize / 2)
		else
			initValue = 1
			finalValue = #path
			step = 1
			self.goal = Vec3(path[#path].x * pathUnitSize - pathUnitSize / 2, 0, path[#path].y * pathUnitSize - pathUnitSize / 2)
		end

		for i = initValue, finalValue, step do
			GiveOrder(assignedUnit, CMD.MOVE, {path[i].x * pathUnitSize - pathUnitSize / 2, Spring.GetGroundHeight(path[i].x * pathUnitSize - pathUnitSize / 2,  path[i].y * pathUnitSize - pathUnitSize / 2), path[i].y * pathUnitSize - pathUnitSize / 2}, {"shift"})
		end
	
		self.initialization = false
	elseif not self.initialization then	-- Periodically checks whether the unit is still alive, whether it is moving and whether it has arrived at its destination.
		local x, y, z = GetUnitPosition(assignedUnit)
		local pos = Vec3(x, 0, z)
	
		if GetUnitIsDead(assignedUnit) == nil or GetUnitIsDead(assignedUnit) then
			return FAILURE
		end
	
		if not ValidUnitID(assignedUnit) then
			return FAILURE
		end
	
		if GetUnitHealth(assignedUnit) <= 0 then
			return FAILURE
		end
	
		if(assignedUnit ~= self.prevID) then
			return FAILURE
		end
	
		if self.prevPos == pos then
			self.threshold = self.threshold + THRESHOLD_STEP
		end
	
		if(pos:Distance(self.goal) < self.threshold) then
			return SUCCESS
		end
		
		self.prevPos = pos
	end
	
	return RUNNING
end

function Reset(self)
	ClearState(self)
end