function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Unload units at a given location.",
		parameterDefs = {
			{
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
		}
	}
end

-- speed-ups
local GiveOrder = Spring.GiveOrderToUnit
local GetPosition = Spring.GetUnitPosition
local NearestEnemy = Spring.GetUnitNearestEnemy
local UnitsInCylinder = Spring.GetUnitsInCylinder
local TransportedUnits = Spring.GetUnitIsTransporting

function Run(self, units, parameters)
	-- Succeeds if the transport unit isn't carrying any units.
	if(#TransportedUnits(units[1]) == 0) then
		return SUCCESS
	end

	-- Starts unloading if the unit idles somewhere around the set destination.
	x, y, z = GetPosition(units[1])
	if(self:UnitIdle(units[1]) and math.abs(x - parameters.x) < 100 and math.abs(z - parameters.z) < 100) then
		Spring.Echo("marko")
		GiveOrder(units[1], CMD.UNLOAD_UNITS, {x, 0, z, 100}, {"shift"})
		return RUNNING
	end
	
	if(self:UnitIdle(units[1])) then
		Spring.Echo("polo")
		GiveOrder(units[1], CMD.MOVE, {parameters.x, 0, parameters.z}, {"shift"})
		return RUNNING
	end
	
	return RUNNING
end