function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Select a given unit, adding it to the units array.",
        parameterDefs = {
            { 
                name = "unitID",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = 0,
            }
        }
    }
end

function Run(self, units, parameters)
	Spring.SelectUnitArray({[1] = parameters.unitID}, true)
	
	return SUCCESS
end