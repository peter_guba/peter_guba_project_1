function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Load units within a given circle.",
		parameterDefs = {
			{
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}			
		}
	}
end

-- speed-ups
local GetPosition = Spring.GetUnitPosition
local UnitsInCylinder = Spring.GetUnitsInCylinder
local GiveOrder = Spring.GiveOrderToUnit
local TransportedUnits = Spring.GetUnitIsTransporting

function Run(self, units, parameters)
	-- Succeeds if there are no units in the given radius which the transport isn't carrying.
	unitCount = #UnitsInCylinder(parameters.x, parameters.z, parameters.radius)
	if (LoadedUnitsCount(units[1]) == unitCount - 1) then
		return SUCCESS
	end
	
	GiveOrder(units[1], CMD.MOVE, {parameters.x, 0, parameters.z}, {"shift"})
	GiveOrder(units[1], CMD.LOAD_UNITS, {parameters.x, 0, parameters.z, parameters.radius}, {"shift"})
	
	return RUNNING
end

-- Gets the amount of units that the transport is carrying.
function LoadedUnitsCount(id)
	transportedUnits = TransportedUnits(id)
		
	if (transportedUnits ~= nil) then 
		return #transportedUnits
	else 
		return 0
	end
end