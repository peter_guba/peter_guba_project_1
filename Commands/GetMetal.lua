function getInfo()
	return {
		onNoUnits = RUNNING,
		tooltip = "Tells units to gather metal in a given area.",
		parameterDefs = {
			{
				name = "x",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "z",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "radius",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			},
			{
				name = "unit",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
		}
	}
end

-- speed-ups
local GiveOrder = Spring.GiveOrderToUnit
local GiveIndividualOrder = Spring.GiveOrderToUnit
local GetUnitCommands = Spring.GetUnitCommands
local GetCommandQueue = Spring.GetCommandQueue

local function ClearState(self)
	self.initialization = false
end

function Run(self, units, parameters)
	if not self.initialization then
		-- GiveOrder(parameters.unit, CMD.MOVE, {parameters.x, 0, parameters.z}, {})
		GiveOrder(parameters.unit, CMD.RECLAIM, {parameters.x, 0, parameters.z, parameters.radius}, {"shift"})
		self.initialization = true
		
		return RUNNING
	else	
		if #GetCommandQueue(parameters.unit) > 0 then
			return RUNNING
		end
	
		return SUCCESS
	end
end

function Reset(self)
	ClearState(self)
end