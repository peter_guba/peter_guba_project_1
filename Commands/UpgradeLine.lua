-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "message") -- communication backend load

function getInfo()
    return {
        onNoUnits = SUCCESS, -- instant success
        tooltip = "Get ifno unit in param",
        parameterDefs = {
            { 
                name = "lineName",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "GoodTop",
            },
            { 
                name = "upgradeLevel",
                variableType = "expression",
                componentType = "editBox",
                defaultValue = "1",
            },
			{
				name = "price",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = 0
			}
        }
    }
end

-- speed-ups
local GetTeamResources = Spring.GetTeamResources

function Run(self, units, parameters)
	if GetTeamResources(0, "metal") < parameters.price then
		return FAILURE
	end

    message.SendRules({
        subject = "swampdota_upgradeLine",
        data = {
            lineName = parameters.lineName,
            upgradeLevel = parameters.upgradeLevel,
        },
    })

    return SUCCESS
end


function Reset(self)
    return self
end