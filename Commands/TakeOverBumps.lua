function getInfo()
	return {
		onNoUnits = SUCCESS, -- instant success
		tooltip = "Dispatches units to all of the bumps.",
		parameterDefs = {
			{
				name = "bumpPositions",
				variableType = "expression",
				componentType = "editBox",
				defaultValue = {}
			}
		}
	}
end

-- speed-ups
local GetType = Spring.GetUnitDefID
local GiveOrderToUnit = Spring.GiveOrderToUnit
local orderGiven = false

function Run(self, units, parameters)
	-- The if statement makes sure that the order is given only once, otherwise the units would stop
	-- moving every time they received the order.
	if not orderGiven then
		-- Dispatch one unit to every bump.
		for i = 1, #parameters.bumpPositions do
			if i < #units then
				GiveOrderToUnit(units[i], CMD.MOVE, {parameters.bumpPositions[i].x, 0, parameters.bumpPositions[i].z}, {"shift"})
			end
		end
		
		orderGiven = true
	end
	
	-- Determines whether all the units have arrived at their respective destinations.
	done = true
	for j = 1, #units do
		if not self:UnitIdle(units[j]) then
			done = false
		end
	end
	
	if done then
		return SUCCESS
	end
	
	return RUNNING
end