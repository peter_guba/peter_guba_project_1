moduleInfo = {
	name = "drawSquaresWidget",
	desc = "Draws squares of given size and with given opacities at given positions.",
	author = "Peter Guba",
	date = "2020-05-26",
	license = "notAlicense",
	layer = -1,
	enabled = true
}

function widget:GetInfo()
	return moduleInfo
end


-- get madatory module operators
VFS.Include("modules.lua") -- modules table
VFS.Include(modules.attach.data.path .. modules.attach.data.head) -- attach lib module

-- get other madatory dependencies
attach.Module(modules, "stringExt")
Vec3 = attach.Module(modules, "vec3")

local spEcho = Spring.Echo
local spAssignMouseCursor = Spring.AssignMouseCursor
local spSetMouseCursor = Spring.SetMouseCursor
local spGetGroundHeight = Spring.GetGroundHeight
local spTraceScreenRay = Spring.TraceScreenRay
local spGetUnitPosition = Spring.GetUnitPosition
local glColor = gl.Color
local glRect = gl.Rect
local glTexture	= gl.Texture
local glDepthTest = gl.DepthTest
local glBeginEnd = gl.BeginEnd
local glPushMatrix = gl.PushMatrix
local glPopMatrix = gl.PopMatrix
local glTranslate = gl.Translate
local glText = gl.Text
local glLineWidth = gl.LineWidth
local glLineStipple = gl.LineStipple
local glVertex = gl.Vertex
local GL_LINE_STRIP = GL.LINE_STRIP
local GL_QUADS = GL.QUADS
local TextDraw = fontHandler.Draw
local max = math.max
local min = math.min

local instances = {}
local width = 0

local function Update(lineID, lineData)
	instances[lineID] = lineData
end

function widget:Initialize()
	widgetHandler:RegisterGlobal('drawSquaresDebug_update', Update)
	instances = {}
end

function widget:GameFrame(n)
end

function widget:DrawWorld()
	gl.PushMatrix()
	for instanceKey, instanceData in pairs(instances) do	
		if (instanceData.point1 ~= nil and instanceData.point2 ~= nil and instanceData.point3 ~= nil and instanceData.point4 ~= nil and instanceData.opacity > 0.1) then
			
			local function Rect(x1, x2, y1, y2)
				glVertex(x1[1], x1[2], x1[3])
				glVertex(x2[1], x2[2], x2[3])
				glVertex(y1[1], y1[2], y1[3])
				glVertex(y2[1], y2[2], y2[3])
			end
			
			glColor(instanceData.r, instanceData.g, instanceData.b, instanceData.opacity)
			gl.DrawGroundQuad(instanceData.point1.x, instanceData.point1.z, instanceData.point3.x, instanceData.point3.z)
		end
	end
	gl.PopMatrix()
	glColor(1, 0, 0, 1)
end